// natural and simple

//node.js -> will provide us with a running environment (RTE) that will allow us to execute our program.

//RTE (RUNTIME Environment) -> is the environemnt/system in which a program can be executed.



// task: lets create a standard server set up plain node JS

//Identify and prepare the components/ingredientd needed in order to execute the task.
	//http (hypertext transfer protocol)
	//http -> is a built in  module of NodeJS. That will allow us to establish a connection and aloow us to transfer data over the http.

	//use a function called require() in order to acquire the desired module. Repackage the module and store it inside a new variable.
let http = require('http');  

	//http will provide us with all the component needed to be establish a server.


	//createServer() will allow us to create HTTP server. provide/instruct the
	//server what to do print a simple message. Identify and describe a
	//locationwhere the connection will happen, in order to provide a proper
	//medium for both parties, and bind the connection to desired port number
let port = 3000;

	//listen() -> bind and listen to a specific port whenever its being accessed by your computer.
	//identify a point where we want to terminate the transmission using the end() function
http.createServer(function(request, response)
{
	response.write("Hello");
	response.end(); //terminate the transmission
}).listen(port);

// create a response in  the termminal to confirm if a connection has been successfully establish
	//1. we can give a confirmation to the client that a connection is working
	//2. we can specify what address or location the connection was established.
	//we will use template literals
console.log(`Server is running with nodemon on port: ${port}`);

//what can we do for us to be able to automatically hotflix the system without restarting the server

	//1. initialize a npm into your local project.
		//syntax: npm init
		//shortcut syntaxt: npm init -y (yes)
	//package.json -> "the heart of any node JS projects". it records all the important metadata about the project, (libraries, packages, functions) that makes up the system.

	//2. install 'nodemon' into our project
		//PRACTICE SKILLS:
		// install:
			//npm install <dependency>
			//npm i <dependency>
		// uninstall:
			//npm uninstall <dependency>
			//npm un <dependency>
		// installing multiple dependencies:
			//npm i <list of dependencies>
				//express	
	//3. run your app using the nodemon utility engine.
		//note: make sure that nodemon is recognized by your file system structure
		//npm install -g nodemon (global)
			//-> library is being installed globbaly into your machines file system so that a program will become recognizable accross your file system.
			// (YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE)
		//nodemon index.js

		//nodemon utility -> is a CLI utility tool, its's task is to wrap your node js app and watched for any changes in the file system and automatically restarts/hotfic the process

		//create your personalized script to execute the app using the nodemon utility.
			//register a new command to start the app.
				//nodemon index.js // shortcut: npm start
				//if you will create own script, use npm run kahitAno
			//start -> common execute scripts for node apps
			//others -> run + command	
//[SEGWEY] Register sublime text 3 as part of your environment variables.

	//1. Locate in your PC where sublime text 3 is installed
	//2. Copy the path where sublime text is installed 
		//C:\Program Files\Sublime Text
	//3. Locate the environment variables in your system